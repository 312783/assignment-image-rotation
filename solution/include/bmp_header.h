#include <stdint.h>
#include <stdbool.h>

#include "dimensions.h"

struct __attribute__((packed)) bmp_header
{
        uint8_t  bfType[2];
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};

uint8_t bmp_header_get_padding(const struct bmp_header *header);
bool bmp_header_is_correct(const struct bmp_header *header);
struct dimensions bmp_header_get_dimensions(const struct bmp_header *header);
