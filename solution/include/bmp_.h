#pragma once

#include "bmp_header.h"
#include "image.h"
#include "io.h"

struct bmp_image {
        struct bmp_header header;
        struct image image;
};

struct bmp_image bmp_load(FILE * file);
bool bmp_is_correct(const struct bmp_image * bmp_image);
bool bmp_save(FILE * file, struct bmp_image * bmp_image);
void bmp_forget(struct bmp_image * bmp_image);
void bmp_update_header(struct bmp_image * bmp_image);
