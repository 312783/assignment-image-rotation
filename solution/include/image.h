#pragma once

#include <stddef.h>
#include <malloc.h>
#include <inttypes.h>
#include <stdbool.h>

#include "dimensions.h"

struct pixel {
  uint8_t components[3];
};

struct image {
  struct dimensions size;
  struct pixel* data;
};

struct image image_create(const struct dimensions size );
void image_forget(struct image * img);
bool image_is_correct(const struct image * img);

struct pixel image_get_pixel(const struct image * image, size_t x, size_t y);
struct pixel * image_pixel(struct image * image, size_t x, size_t y);
