#pragma once

#include "image.h"

struct image image_rotate_90_counterclockwise(const struct image * source);
