#pragma once

#include <stdio.h>

FILE * file_open_read(const char* path);
FILE * file_open_write(const char* path);
void file_close(FILE * file);

void print_error(const char* text);
