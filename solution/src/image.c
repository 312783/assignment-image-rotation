#include <stdbool.h>

#include "image.h"

struct image image_create(const struct dimensions size ) {
    struct pixel* const data = malloc(size.x*size.y*sizeof(struct pixel));
    struct image img = {
        .data = data,
        .size = size
    };
    return img;
}

void image_forget(struct image * img ) {
    if (!img)
        return;
    free(img->data);
    img->data = NULL;
    img->size.x = 0;
    img->size.y = 0;
}

bool image_is_correct(const struct image * img) {
    return img && img->data && img->size.x > 0 && img->size.y > 0;
}

struct pixel image_get_pixel(const struct image * image, size_t x, size_t y) {
    const size_t pos = y * image->size.x + x;
    return image->data[pos];
}

struct pixel * image_pixel(struct image * image, size_t x, size_t y) {
    const size_t pos = y * image->size.x + x;
    return image->data + pos;
}
