#include <stdio.h>

#include "bmp_.h"
#include "io.h"
#include "rotation.h"

#define EXECUTABLE_NAME "image-transformer"

int main( int argc, char** argv ) {
    if (argc != 3) {
        print_error("Usage: ./" EXECUTABLE_NAME " BMP_FILE_NAME1 BMP_FILE_NAME2\n");
        return 1;
    }

    FILE * file_to_read = file_open_read(argv[1]);
    if (!file_to_read) {
        print_error("reading bmp opening error\n");
        return 2;
    }

    FILE * file_to_write = file_open_write(argv[2]);
    if (!file_to_write) {
        print_error("writing bmp opening error\n");
        return 3;
    }

    struct bmp_image img = bmp_load(file_to_read);
    file_close(file_to_read);

    if (!bmp_is_correct(&img)) {
        print_error("bmp is not loaded correctly\n");
        return 4;
    }

    struct image image_source = img.image;
    img.image = image_rotate_90_counterclockwise(&image_source);
    image_forget(&image_source);

    const bool bmp_save_result = bmp_save(file_to_write, &img);
    file_close(file_to_write);

    if (!bmp_save_result) {
        print_error("bmp is not saved correctly\n");
        return 5;
    }
    
    bmp_forget(&img);
    return 0;
}
