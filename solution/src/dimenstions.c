#include "dimensions.h"

struct dimensions dimensions_reverse( const struct dimensions* dim ) {
    return (struct dimensions) {.x=dim->y, .y=dim->x};
}
