#include "bmp_.h" // WHY IF I NAME THIS FILE 'bmp.h' STRUCT bmp_header SEEMS UNDEFINED?!

struct bmp_image bmp_load(FILE * file) {
    struct bmp_image bmp_image = {0};
    const size_t header_read_parts = fread(&bmp_image.header, sizeof(struct bmp_header), 1, file);
    if (!header_read_parts || !bmp_header_is_correct(&bmp_image.header)) {
        return (struct bmp_image){0}; // wrong header
    }
    const struct dimensions dims = bmp_header_get_dimensions(&bmp_image.header);
    bmp_image.image = image_create(dims);
    if (!image_is_correct(&bmp_image.image)) {
        return (struct bmp_image){0}; // image creation error
    }
    const uint8_t padding = bmp_header_get_padding(&bmp_image.header);
    const size_t predicted_row_bytes = bmp_image.image.size.x * sizeof(struct pixel);
    size_t row_bytes; 
    struct pixel * data_pointer = bmp_image.image.data;
    for (size_t y = 0; y < bmp_image.image.size.y; ++y) {
        row_bytes = fread(data_pointer, 1, predicted_row_bytes, file);
        const int fseek_result = fseek(file, padding, SEEK_CUR);
        if (predicted_row_bytes != row_bytes || fseek_result != 0) {
            return (struct bmp_image){0}; // wrong bytes count
        }
        data_pointer += bmp_image.image.size.x;
    }
    return bmp_image;
}

bool bmp_is_correct(const struct bmp_image * bmp_image) {
    return bmp_header_is_correct(&bmp_image->header) && image_is_correct(&bmp_image->image);
}

bool bmp_save(FILE * file, struct bmp_image * bmp_image) {
    bmp_update_header(bmp_image);
    const size_t header_writed_parts = fwrite(&bmp_image->header, sizeof(struct bmp_header), 1, file);
    if (!header_writed_parts) {
        return false; // header writing error
    }
    const uint8_t padding = bmp_header_get_padding(&bmp_image->header);
    const size_t predicted_row_bytes = bmp_image->image.size.x * sizeof(struct pixel);
    size_t row_writed_bytes;
    struct pixel * data_pointer = bmp_image->image.data;
    const uint64_t zero = 0;
    for (size_t y = 0; y < bmp_image->image.size.y; ++y) {
        row_writed_bytes = fwrite(data_pointer, 1, predicted_row_bytes, file);
        fwrite(&zero, 1, padding, file);
        if (predicted_row_bytes != row_writed_bytes ) {
            return false; // wrong bytes count
        }
        data_pointer += bmp_image->image.size.x;
    }
    return true;
}

void bmp_forget(struct bmp_image * bmp_image) {
    bmp_image->header = (struct bmp_header) {0};
    image_forget(&bmp_image->image);
}

void bmp_update_header(struct bmp_image* bmp_image) {
    bmp_image->header.biWidth = bmp_image->image.size.x;
    bmp_image->header.biHeight = bmp_image->image.size.y;
    bmp_image->header.bfileSize = sizeof(struct bmp_header) + (bmp_image->image.size.x * sizeof(struct pixel) + bmp_header_get_padding(&bmp_image->header)) * bmp_image->image.size.y;
    // TODO find other values to update (they still exist)
}
