#include "rotation.h"

struct image image_rotate_90_counterclockwise(const struct image * source) {
    struct image result = image_create(dimensions_reverse(&source->size));
    for (size_t y = 0; y < result.size.y; ++y) {
        for (size_t x = 0; x < result.size.x; ++x) {
            const size_t src_x = y;
            const size_t src_y = result.size.x - x - 1;
            *image_pixel(&result, x, y) = image_get_pixel(source, src_x, src_y);
        }
    }
    return result;
}
