#include "bmp_header.h"

uint8_t bmp_header_get_padding(const struct bmp_header *header) {
    return header->biWidth % 4;
}

bool bmp_header_is_correct(const struct bmp_header *header) {
    if (header->bfType[0] != 'B' || header->bfType[1] != 'M')
        return false;
    if (header->biBitCount != 24)
        return false;
    return true;
}

struct dimensions bmp_header_get_dimensions(const struct bmp_header *header) {
    return (struct dimensions){.x=header->biWidth, .y=header->biHeight};
}
