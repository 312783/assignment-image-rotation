#include <stdio.h>

#include "io.h"

FILE * file_open_read(const char* path) {
    return fopen(path, "rb");
}

FILE * file_open_write(const char* path) {
    return fopen(path, "wb");
}

void file_close(FILE * file) {
    if (file)
        fclose(file);
}

void print_error(const char* text) {
    fprintf(stderr, "%s\n", text);
}
